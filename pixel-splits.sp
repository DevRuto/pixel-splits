#define PLUGIN_NAME "Pixel - Splits"
#define PLUGIN_AUTHOR "Ruto"
#define PLUGIN_VERSION "1.0"
#define PREFIX "SPL"

#define MAX_SETS                10
#define C_WHITE					{ 255, 255, 255, 255 }
#define C_GREEN					{   0, 255,   0, 255 }
#define C_RED					{ 255,   0,   0, 255 }

#define C_ACTIVE                C_RED
#define C_ZONE                  C_GREEN
#define C_SQUARE                C_WHITE

#include <sourcemod>
#include <sdktools>
#include <sourcemod-colors>
#include <gamechaos>
#include <pixel-splits>

#undef REQUIRE_PLUGIN
#include <gokz/core>
#include <kztimer>

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = "Splits plugin",
	version = PLUGIN_VERSION,
	url = ""
};

#pragma newdecls required
#pragma semicolon 1

int gI_Beam;

Database gH_DB = null;
int gI_MapID = -1;

int gI_Set[MAXPLAYERS + 1] = 0;
ArrayList gA_Zones[MAXPLAYERS + 1][MAX_SETS];
ArrayList gA_Times[MAXPLAYERS + 1];

int gI_CurrentZone[MAXPLAYERS + 1] = 0;
Zone gZ_Zone[MAXPLAYERS + 1];

#include "pixel-splits/zone/main.sp"
#include "pixel-splits/zone/add_zone.sp"
#include "pixel-splits/zone/edit_zone.sp"

#include "pixel-splits/splits/main.sp"

#include "pixel-splits/db/sql.sp"
#include "pixel-splits/db/error.sp"
#include "pixel-splits/db/setup_database.sp"
#include "pixel-splits/db/create_tables.sp"
#include "pixel-splits/db/setup_map.sp"
#include "pixel-splits/db/setup_client.sp"
#include "pixel-splits/db/add_zone.sp"
#include "pixel-splits/db/update_zone.sp"
#include "pixel-splits/db/delete_zone.sp"

#include "pixel-splits/commands.sp"
#include "pixel-splits/forwards.sp"
#include "pixel-splits/natives.sp"
#include "pixel-splits/kz_hooks.sp"

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
    if (GetEngineVersion() != Engine_CSGO)
    {
        SetFailState("Pixel Splits only supports CS:GO servers.");
    }

    CreateNatives();
    RegPluginLibrary("pixel-splits");
    return APLRes_Success;
}

public void OnPluginStart()
{
    CSetPrefix(PREFIX);
    CreateCommands();
    CreateForwards();
}

public void OnAllPluginsLoaded()
{
    DB_SetupDatabase();
}

public void OnConfigsExecuted()
{
	gI_Beam = PrecacheModel("materials/sprites/laser.vmt", true);
}

public void OnMapStart()
{
    DB_SetupMap();
}

public void OnClientAuthorized(int client, const char[] auth)
{
	DB_SetupClient(client);
}

public void OnPlayerRunCmdPost(int client, int buttons, int impulse, const float vel[3], const float angles[3], int weapon, int subtype, int cmdnum, int tickcount, int seed, const int mouse[2])
{
    float fPlayerMins[3];
    float fPlayerMaxs[3];
    GetClientMins(client, fPlayerMins);
    GetClientMaxs(client, fPlayerMaxs);
    float fPlayerOrigin[3];
    GetClientAbsOrigin(client, fPlayerOrigin);
    for (int i; i < 3; i++)
    {
        fPlayerMins[i] += fPlayerOrigin[i];
        fPlayerMaxs[i] += fPlayerOrigin[i];
    }

    int currentSet = gI_Set[client];
    ArrayList zoneSet = gA_Zones[client][currentSet];
    if (zoneSet == null || zoneSet.Length == 0 || gI_CurrentZone[client] == zoneSet.Length) return; 

    if (!(AABBIntersecting(fPlayerMins, fPlayerMaxs, gZ_Zone[client].mins, gZ_Zone[client].maxs)))
    {
        return;
    }

    if (!IsTimerRunning(client)) return;

    // Zone passed
    Splits_Split(client, GetCurrentTime(client));
}

public void Pixel_OnMapSetup(int mapId)
{
    PrintToServer("[SPLITS] Initialised - Map ID: %i", mapId);
    char auth[32];
    for (int client = 1; client <= MaxClients; client++)
    {
        if (IsClientAuthorized(client) && !IsFakeClient(client) && GetClientAuthId(client, AuthId_Engine, auth, sizeof(auth)))
        {
            OnClientAuthorized(client, auth);
        }
    }

    CreateTimer(0.5, Timer_ShowZones, _, TIMER_REPEAT);
}

public void Pixel_OnClientSetup(int client)
{
    CPrintToChat(client, "Zones Loaded");
}

public void Pixel_OnZoneAdded(int client, int set, int zone_index, Zone zone)
{
    CPrintToChat(client, "Zone %i added to Set %i", zone_index + 1, set + 1);
    DB_AddZone(client, set, zone_index, zone);
}

public void Pixel_OnZoneEdited(int client, int set, int index, Zone zone)
{
    CPrintToChat(client, "Zone %i Updated from Set %i", index + 1, set + 1);
    DB_UpdateZone(client, set, index, zone);
}

public void Pixel_OnZoneDeleted(int client, int set, int index)
{
    CPrintToChat(client, "Zone %i deleted from Set %i", index + 1, set + 1);
    DB_DeleteZone(client, set, index);
}

public Action Timer_ShowZones(Handle timer)
{
    for (int client = 1; client <= MaxClients; client++)
    {
        if (!IsClientConnected(client) || IsFakeClient(client) || !IsClientInGame(client) || IsClientObserver(client)) continue;
        // if (IsTimerRunning(client)) continue;
        ArrayList zones = gA_Zones[client][gI_Set[client]];
        if (zones == null || zones.Length == 0) continue;
        for (int zoneIndex = 0; zoneIndex < zones.Length; zoneIndex++)
        {
            Zone zone;
            zones.GetArray(zoneIndex, zone, sizeof(zone));
            zone.SendBeamZone(client, gI_Beam, gI_CurrentZone[client] == zoneIndex ? C_RED : C_GREEN);
        }
    }
    return Plugin_Continue;
}

public void Pixel_OnSplitStart(int client, int set)
{
    CPrintToChat(client, "Split started on set %i", set);
}

public void Pixel_OnSplit(int client, int set, int zoneIndex, float spltTime, float totalTime)
{
    CPrintToChat(client, "Split: %f - %s", spltTime, GOKZ_FormatTime(totalTime));
}

public void Pixel_OnSplitFinish(int client, int set, int zoneIndex, int numSplits, const float[] splits, const float[] times)
{
    PrintToConsole(client, "=== Split Results for %N ===", client);
    for (int i = 0; i < numSplits; i++)
    {
        PrintToConsole(client, "  %i. %f - %f", i + 1, splits[i], times[i]);
    }
}

bool IsTimerRunning(int client)
{
	if (GetFeatureStatus(FeatureType_Native, "KZTimer_GetTimerStatus") == FeatureStatus_Available
		&& KZTimer_GetTimerStatus(client))
	{
		return true;
	}
	else if (GetFeatureStatus(FeatureType_Native, "GOKZ_GetTimerRunning") == FeatureStatus_Available
		&& GOKZ_GetTimerRunning(client))
	{
		return true;
	}
	else
	{
		return false;
	}
}

float GetCurrentTime(int client)
{
    if (!IsTimerRunning(client)) return -1.0;
    if (GetFeatureStatus(FeatureType_Native, "GOKZ_GetTime") == FeatureStatus_Available)
	{
		return GOKZ_GetTime(client);
	} else if (GetFeatureStatus(FeatureType_Native, "KZTimer_GetCurrentTime") == FeatureStatus_Available)
    {
        return KZTimer_GetCurrentTime(client);
    }
    return -1.0;
}