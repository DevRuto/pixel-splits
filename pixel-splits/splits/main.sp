static float startTime[MAXPLAYERS + 1];

void Splits_Start(int client)
{
    ArrayList zones = gA_Zones[client][gI_Set[client]];
    if (zones == null || zones.Length == 0) return;

    if (gA_Times[client] == null)
        gA_Times[client] = new ArrayList(2);

    gI_CurrentZone[client] = 0;
    gA_Times[client].Resize(0);

    zones.GetArray(0, gZ_Zone[client]);

    Call_OnSplitStart(client, gI_Set[client]);

    startTime[client] = GetGameTime();
}

void Splits_Finish(int client, float finalTime)
{
    Splits_Split(client, finalTime);
    ArrayList arrTimes = gA_Times[client];
    float[] splits = new float[arrTimes.Length];
    float[] times = new float[arrTimes.Length];
    for (int i = 0; i < arrTimes.Length; i++)
    {
        splits[i] = arrTimes.Get(i, 0);
        times[i] = arrTimes.Get(i, 1);
    }
    Call_OnSplitFinish(client, gI_Set[client], gI_CurrentZone[client], arrTimes.Length, splits, times);
}

void Splits_Split(int client, float currentTime)
{
    float time = GetGameTime();
    float splitTime = time - startTime[client];
    float block[2];
    block[0] = splitTime;
    block[1] = currentTime;
    gA_Times[client].PushArray(block);

    Call_OnSplit(client, gI_Set[client], gI_CurrentZone[client], splitTime, currentTime);
    gI_CurrentZone[client]++;
    if (gI_CurrentZone[client] < gA_Zones[client][gI_Set[client]].Length)
    gA_Zones[client][gI_Set[client]].GetArray(gI_CurrentZone[client], gZ_Zone[client]);
    startTime[client] = GetGameTime();
}