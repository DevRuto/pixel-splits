void CreateNatives()
{
    CreateNative("Pixel_GetMapID", Native_GetMapID);
    CreateNative("Pixel_GetCurrentSetIndex", Native_GetCurrentSetIndex);
    CreateNative("Pixel_GetCurrentSet", Native_GetCurrentSet);
}

public int Native_GetMapID(Handle plugin, int numParams)
{
    return gI_MapID;
}

public int Native_GetCurrentSetIndex(Handle plugin, int numParams)
{
    return gI_Set[GetNativeCell(1)];
}

public int Native_GetCurrentSet(Handle plugin, int numParams)
{
    int client = GetNativeCell(1);
    return view_as<int>(gA_Zones[client][gI_Set[client]]);
}