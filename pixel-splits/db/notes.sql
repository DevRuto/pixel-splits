CREATE TABLE pixel_maps (
    id INTEGER PRIMARY KEY /*!40101 AUTO_INCREMENT */,
    map TEXT NOT NULL
);

CREATE TABLE pixel_zones (
    player INTEGER,
    map_id INTEGER,
    set_index INTEGER,
    zone_index INTEGER,
    point1_x INTEGER,
    point1_y INTEGER,
    point1_z INTEGER,
    point2_x INTEGER,
    point2_y INTEGER,
    point2_z INTEGER,
    min1 INTEGER,
    min2 INTEGER,
    min3 INTEGER,
    max1 INTEGER,
    max2 INTEGER,
    max3 INTEGER,
    PRIMARY KEY (player, map_id, set_index)
)

CREATE TABLE pixel_splits (
    player INTEGER,
    map_id INTEGER,
    set_index INTEGER,
    zone_index INTEGER,
    time float,
    PRIMARY KEY (player, map_id, set_index, zone_index)
)