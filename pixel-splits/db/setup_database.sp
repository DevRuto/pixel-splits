void DB_SetupDatabase()
{
    char error[255];
    gH_DB = SQL_Connect("pixel", true, error, sizeof(error));
    if (gH_DB == null)
    {
        SetFailState("Unable to connect to database. Error: %s", error);
    }

    DB_CreateTables();
}