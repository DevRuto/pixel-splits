void DB_SetupMap()
{
    char map[PLATFORM_MAX_PATH];
    GetCurrentMap(map, sizeof(map));
    GetMapDisplayName(map, map, sizeof(map));

    Transaction txn = SQL_CreateTransaction();

    char databaseType[8];
    SQL_ReadDriver(gH_DB, databaseType, sizeof(databaseType));
    char query[200];
    if (strcmp(databaseType, "sqlite", false) == 0)
    {
        FormatEx(query, sizeof(query), sqlite_insert_map, map);
    }
    else
    {
        FormatEx(query, sizeof(query), mysql_insert_map, map);
    }
    txn.AddQuery(query);

    FormatEx(query, sizeof(query), sql_select_mapid, map);
    txn.AddQuery(query);

    SQL_ExecuteTransaction(gH_DB, txn, DB_OnMapSetup, DB_TxnFailure_Generic, _, DBPrio_High);
}

public void DB_OnMapSetup(Handle db, any data, int numQueries, Handle[] results, any[] queryData)
{
    SQL_FetchRow(results[1]);
    gI_MapID = SQL_FetchInt(results[1], 0);
    Call_OnMapSetup(gI_MapID);
}