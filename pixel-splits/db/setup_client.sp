void DB_SetupClient(int client)
{
    int accountId = GetSteamAccountID(client);
    
    for (int i = 0; i < MAX_SETS; i++)
    {
        if (gA_Zones[client][i] == null)
            gA_Zones[client][i] = new ArrayList(sizeof(Zone));
    }

    Transaction txn = SQL_CreateTransaction();

    char query[1024];
    FormatEx(query, sizeof(query), sql_select_zones, accountId, gI_MapID);
    txn.AddQuery(query);

    SQL_ExecuteTransaction(gH_DB, txn, DB_OnClientSetup, DB_TxnFailure_Generic, client, DBPrio_High);
}

public void DB_OnClientSetup(Handle db, any data, int numQueries, Handle[] results, any[] queryData)
{
    int client = data;
    Handle result = results[0];
    while (SQL_FetchRow(result))
    {
        int set_index = SQL_FetchInt(result, 0);
        int zone_index = SQL_FetchInt(result, 1);

        Zone zone;
        for (int i = 0; i < 3; i++)
        {
            zone.point1[i] = SQL_FetchFloat(result, 2 + i);
            zone.point2[i] = SQL_FetchFloat(result, 5 + i);
            zone.mins[i] = SQL_FetchFloat(result, 8 + i);
            zone.maxs[i] = SQL_FetchFloat(result, 11 + i);
        }
        zone.point1_active = true;
        zone.point2_active = true;

        ArrayList zones = gA_Zones[client][set_index];
        if ((zone_index + 1) >= zones.Length)
            zones.Resize(zone_index + 1);
        zones.SetArray(zone_index, zone);
    }

    Call_OnClientSetup(client);

    delete result;
}