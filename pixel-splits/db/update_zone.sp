void DB_UpdateZone(int client, int set, int zone_index, Zone zone)
{
    int accountId = GetSteamAccountID(client);

    Transaction txn = SQL_CreateTransaction();

    char query[1024];

    Format(
        query,
        sizeof(query),
        sql_update_zone,
        zone.point1[0],
        zone.point1[1],
        zone.point1[2],
        zone.point2[0],
        zone.point2[1],
        zone.point2[2],
        zone.mins[0],
        zone.mins[1],
        zone.mins[2],
        zone.maxs[0],
        zone.maxs[1],
        zone.maxs[2],
        accountId, gI_MapID, set, zone_index);
    txn.AddQuery(query);

    SQL_ExecuteTransaction(gH_DB, txn, _, DB_TxnFailure_Generic);
}