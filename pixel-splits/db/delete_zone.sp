void DB_DeleteZone(int client, int set, int zone_index)
{
    int accountId = GetSteamAccountID(client);

    Transaction txn = SQL_CreateTransaction();

    char query[1024];
    FormatEx(query, sizeof(query), sql_delete_zone, accountId, gI_MapID, set, zone_index);
    txn.AddQuery(query);

    FormatEx(query, sizeof(query), sql_shift_zones, accountId, gI_MapID, set, zone_index);
    txn.AddQuery(query);

    SQL_ExecuteTransaction(gH_DB, txn, _, DB_TxnFailure_Generic);
}