void DB_CreateTables()
{
    Transaction txn = SQL_CreateTransaction();

    txn.AddQuery(sql_create_maps_table);
    txn.AddQuery(sql_create_zones_table);
    txn.AddQuery(sql_create_splits_table);

    SQL_ExecuteTransaction(gH_DB, txn, _, DB_TxnFailure_Generic, _, DBPrio_High);
}