// https://stackoverflow.com/a/41028314
char sql_create_maps_table[] = 
"CREATE TABLE IF NOT EXISTS pixel_maps ("
..."id INTEGER PRIMARY KEY /*!40101 AUTO_INCREMENT */,"
..."map VARCHAR(32) NOT NULL UNIQUE"
...")";

char sql_create_zones_table[] =
"CREATE TABLE IF NOT EXISTS pixel_zones ("
..."player INTEGER,"
..."map_id INTEGER,"
..."set_index INTEGER,"
..."zone_index INTEGER,"
..."point1_x INTEGER,"
..."point1_y INTEGER,"
..."point1_z INTEGER,"
..."point2_x INTEGER,"
..."point2_y INTEGER,"
..."point2_z INTEGER,"
..."min1 INTEGER,"
..."min2 INTEGER,"
..."min3 INTEGER,"
..."max1 INTEGER,"
..."max2 INTEGER,"
..."max3 INTEGER,"
..."PRIMARY KEY (player, map_id, set_index, zone_index)"
...")";

char sql_create_splits_table[] =
"CREATE TABLE IF NOT EXISTS pixel_splits ("
..."player INTEGER,"
..."map_id INTEGER,"
..."set_index INTEGER,"
..."zone_index INTEGER,"
..."time float,"
..."PRIMARY KEY (player, map_id, set_index, zone_index)"
...")";

char mysql_insert_map[] =
"INSERT IGNORE INTO pixel_maps (map) VALUES ('%s')";
char sqlite_insert_map[] =
"INSERT OR IGNORE INTO pixel_maps (map) VALUES ('%s')";

char sql_select_mapid[] =
"SELECT id from pixel_maps WHERE map LIKE '%s'";

char sql_select_zones[] = ""
..."SELECT "
..."    set_index, zone_index, "
..."    point1_x, point1_y, point1_z, "
..."    point2_x, point2_y, point2_z, "
..."    min1, min2, min3, "
..."    max1, max2, max3 "
..."FROM "
..."    pixel_zones "
..."WHERE "
..."    player = %i AND "
..."    map_id = %i "
..."ORDER BY set_index, zone_index";

char sql_insert_zone[] = ""
..."INSERT INTO pixel_zones ("
..."player, map_id, set_index, zone_index, "
..."point1_x, point1_y, point1_z, "
..."point2_x, point2_y, point2_z, "
..."min1, min2, min3, max1, max2, max3) "
..."VALUES (%i, %i, %i, %i, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f)";

char sql_update_zone[] = ""
..."UPDATE pixel_zones SET "
..."point1_x = %f, point1_y = %f, point1_z = %f, "
..."point2_x = %f, point2_y = %f, point2_z = %f, "
..."min1 = %f, min2 = %f, min3 = %f, "
..."max1 = %f, max2 = %f, max3 = %f "
..."WHERE player = %i AND map_id = %i AND set_index = %i AND zone_index = %i";


char sql_delete_zone[] = 
"DELETE FROM pixel_zones WHERE player = %i AND map_id = %i AND set_index = %i AND zone_index = %i";

char sql_shift_zones[] =
"UPDATE pixel_zones SET zone_index = zone_index - 1 WHERE player = %i AND map_id = %i AND set_index = %i AND zone_index > %i";