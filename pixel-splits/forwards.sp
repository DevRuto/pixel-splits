static Handle H_OnZoneAdded;
static Handle H_OnZoneEdited;
static Handle H_OnZoneDeleted;
static Handle H_OnMapSetup;
static Handle H_OnClientSetup;
static Handle H_OnSplitStart;
static Handle H_OnSplit;
static Handle H_OnSplitFinish;

void CreateForwards()
{
    H_OnZoneAdded   = CreateGlobalForward("Pixel_OnZoneAdded", ET_Ignore, Param_Cell, Param_Cell, Param_Cell, Param_Array);
    H_OnZoneEdited  = CreateGlobalForward("Pixel_OnZoneEdited", ET_Ignore, Param_Cell, Param_Cell, Param_Cell, Param_Array);
    H_OnZoneDeleted = CreateGlobalForward("Pixel_OnZoneDeleted", ET_Ignore, Param_Cell, Param_Cell, Param_Cell);

    H_OnMapSetup    = CreateGlobalForward("Pixel_OnMapSetup", ET_Ignore, Param_Cell);
    H_OnClientSetup = CreateGlobalForward("Pixel_OnClientSetup", ET_Ignore, Param_Cell);

    H_OnSplitStart  = CreateGlobalForward("Pixel_OnSplitStart", ET_Ignore, Param_Cell, Param_Cell);
    H_OnSplit       = CreateGlobalForward("Pixel_OnSplit", ET_Ignore, Param_Cell, Param_Cell, Param_Cell, Param_Cell, Param_Cell);
    H_OnSplitFinish = CreateGlobalForward("Pixel_OnSplitFinish", ET_Ignore, Param_Cell, Param_Cell, Param_Cell, Param_Cell, Param_Array, Param_Array);
}

void Call_OnZoneAdded(int client, int set, int index, Zone zone)
{
    Call_StartForward(H_OnZoneAdded);
    Call_PushCell(client);
    Call_PushCell(set);
    Call_PushCell(index);
    Call_PushArray(zone, sizeof(zone));
    Call_Finish();
}

void Call_OnZoneEdited(int client, int set, int index, Zone zone)
{
    Call_StartForward(H_OnZoneEdited);
    Call_PushCell(client);
    Call_PushCell(set);
    Call_PushCell(index);
    Call_PushArray(zone, sizeof(zone));
    Call_Finish();
}

void Call_OnZoneDeleted(int client, int set, int index)
{
    Call_StartForward(H_OnZoneDeleted);
    Call_PushCell(client);
    Call_PushCell(set);
    Call_PushCell(index);
    Call_Finish();
}

void Call_OnMapSetup(int mapId)
{
    Call_StartForward(H_OnMapSetup);
    Call_PushCell(mapId);
    Call_Finish();
}

void Call_OnClientSetup(int client)
{
    Call_StartForward(H_OnClientSetup);
    Call_PushCell(client);
    Call_Finish();
}

void Call_OnSplitStart(int client, int set)
{
    Call_StartForward(H_OnSplitStart);
    Call_PushCell(client);
    Call_PushCell(set);
    Call_Finish();
}

void Call_OnSplit(int client, int set, int zoneIndex, float splitTime, float totalTime)
{
    Call_StartForward(H_OnSplit);
    Call_PushCell(client);
    Call_PushCell(set);
    Call_PushCell(zoneIndex);
    Call_PushCell(splitTime);
    Call_PushCell(totalTime);
    Call_Finish();
}

void Call_OnSplitFinish(int client, int set, int zoneIndex, int numFinish, const float[] splits, const float[] times)
{
    Call_StartForward(H_OnSplitFinish);
    Call_PushCell(client);
    Call_PushCell(set);
    Call_PushCell(zoneIndex);
    Call_PushCell(numFinish);
    Call_PushArray(splits, numFinish);
    Call_PushArray(times, numFinish);
    Call_Finish();
}