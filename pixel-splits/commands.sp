void CreateCommands()
{
    RegConsoleCmd("sm_zmenu", Command_ZoneMenu, "Open the Zones menu");
    RegConsoleCmd("sm_zmenu_add", Command_ZoneMenu_Add, "Open the Add Zones menu");
    RegConsoleCmd("sm_zmenu_edit", Command_ZoneMenu_Edit, "Open the Edit Zones menu");

    RegConsoleCmd("sm_zdebug", Command_ZDebug);
}

public Action Command_ZoneMenu(int client, int args)
{
    DisplayMainMenu(client);
    return Plugin_Handled;
}

public Action Command_ZoneMenu_Add(int client, int args)
{
    DisplayAddMenu(client);
    return Plugin_Handled;
}

public Action Command_ZoneMenu_Edit(int client, int args)
{
    DisplayEditMenu(client);
    return Plugin_Handled;
}

public Action Command_ZDebug(int client, int args)
{
    PrintToChat(client, "[GC] Check your console");
    PrintToConsole(client, "======== SPLITS INFO FOR %N ========", client);
    for (int s = 0; s < MAX_SETS;s++)
    {
        ArrayList zones = gA_Zones[client][s];
        if (zones == null || zones.Length == 0) break;
        PrintToConsole(client, "-- Set %i - %i zones", s, zones.Length);
        
        for (int i = 0; i < zones.Length; i++)
        {
            Zone zone;
            zones.GetArray(i, zone, sizeof(zone));
            PrintToConsole(client, "    %i. %s", i, ZoneToInfoString(zone));
        }
    }
    return Plugin_Handled;
}