static Zone zone[MAXPLAYERS + 1];

void DisplayAddMenu(int client)
{
    Menu menu = new Menu(MenuHandler_Add);
    menu.Pagination = MENU_NO_PAGINATION;

    char title[32];
    FormatEx(title, sizeof(title), "Add Zone - Set %i\nZone %i", gI_Set[client] + 1, gA_Zones[client][gI_Set[client]].Length + 1);
    menu.SetTitle(title);

    menu.AddItem("point_1", "Set Point 1");
    menu.AddItem("point_2", "Set Point 2");
    menu.AddItem("show", "Show box", zone[client].point1_active && zone[client].point2_active ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
    menu.AddItem("save", "Save box", zone[client].point1_active && zone[client].point2_active ? ITEMDRAW_DEFAULT: ITEMDRAW_DISABLED);
    menu.AddItem("reset", "Reset");
    menu.AddItem("", "", ITEMDRAW_SPACER);
    menu.AddItem("back", "Back");

    menu.Display(client, MENU_TIME_FOREVER);
}

int MenuHandler_Add(Menu menu, MenuAction action, int client, int choice)
{
    if (action == MenuAction_End)
        delete menu;
    if (action != MenuAction_Select) return;

    switch (choice)
    {
        case 0: // Set Point 1
        {
            zone[client].SetPoint1(client);
            zone[client].SendPoint1Square(client, gI_Beam, C_SQUARE);
        }
        case 1: // Set Point 2
        {
            zone[client].SetPoint2(client);
            zone[client].SendPoint2Square(client, gI_Beam, C_SQUARE);
        }
        case 2: // Show Box
        {
            zone[client].SendBeamZone(client, gI_Beam, C_ZONE);
        }
        case 3: // Reset
        {
            if (!zone[client].point1_active)
                CPrintToChat(client, "You did not set point 1");
            else if (!zone[client].point2_active)
                CPrintToChat(client, "You did not set point 2");
            else
            {
                ArrayList zones = gA_Zones[client][gI_Set[client]];
                zones.PushArray(zone[client]);
                Call_OnZoneAdded(client, gI_Set[client], zones.Length - 1, zone[client]);
                zone[client].Reset();
            }
        }
        case 4: // Save
        {
            zone[client].Reset();
        }
        case 6:
        {
            DisplayMainMenu(client);
            return;
        }
    }
    DisplayAddMenu(client);
}