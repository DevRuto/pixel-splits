static Zone editZone[MAXPLAYERS + 1];
// static int gI_CurrentZone[MAXPLAYERS + 1] = 0;

void DisplayEditMenu(int client)
{
    ArrayList zones = gA_Zones[client][gI_Set[client]];
    if (zones.Length == 0)
    {
        CPrintToChat(client, "You have no zones to edit in this set");
        DisplayMainMenu(client);
        return;
    }

    if (!editZone[client].point1_active && !editZone[client].point2_active)
    {
        if (gI_CurrentZone[client] == zones.Length)
            gI_CurrentZone[client]--;
        zones.GetArray(gI_CurrentZone[client], editZone[client]);
    }

    Menu menu = new Menu(MenuHandler_Edit);
    menu.Pagination = MENU_NO_PAGINATION;

    char title[24];
    FormatEx(title, sizeof(title), "Edit Zone %i", gI_CurrentZone[client] + 1);
    menu.SetTitle(title);
    
    menu.AddItem("point_1", "Set Point 1");
    menu.AddItem("point_2", "Set Point 2");
    menu.AddItem("show", "Show box", editZone[client].point1_active && editZone[client].point2_active ? ITEMDRAW_DEFAULT: ITEMDRAW_DISABLED);
    menu.AddItem("save", "Save box", editZone[client].point1_active && editZone[client].point2_active ? ITEMDRAW_DEFAULT: ITEMDRAW_DISABLED);
    menu.AddItem("next", "Next Zone", gI_CurrentZone[client] + 1 == zones.Length ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
    menu.AddItem("prev", "Prev Zone", gI_CurrentZone[client] == 0 ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
    menu.AddItem("back", "Back");
    menu.AddItem("", "", ITEMDRAW_SPACER);
    menu.AddItem("delete", "Delete Zone");

    menu.Display(client, MENU_TIME_FOREVER);
}

int MenuHandler_Edit(Menu menu, MenuAction action, int client, int choice)
{
    if (action == MenuAction_End)
        delete menu;
    if (action != MenuAction_Select) return;

    switch (choice)
    {
        case 0: // Set Point 1
        {
            editZone[client].SetPoint1(client);
            editZone[client].SendPoint1Square(client, gI_Beam, C_SQUARE);
        }
        case 1: // Set Point 2
        {
            editZone[client].SetPoint2(client);
            editZone[client].SendPoint2Square(client, gI_Beam, C_SQUARE);
        }
        case 2: // Show Box
        {
            editZone[client].SendBeamZone(client, gI_Beam, C_ZONE);
        }
        case 3: // Save
        {
            ArrayList zones = gA_Zones[client][gI_Set[client]];
            zones.SetArray(gI_CurrentZone[client], editZone[client]);
            Call_OnZoneEdited(client, gI_Set[client], gI_CurrentZone[client], editZone[client]);
            DisplayMainMenu(client);
            return;
        }
        case 4: // Next Zone
        {
            editZone[client].Reset();
            gI_CurrentZone[client]++;
        }
        case 5: // Prev Zone
        {
            editZone[client].Reset();
            gI_CurrentZone[client]--;
        }
        case 8: // Delete
        {
            ArrayList zones = gA_Zones[client][gI_Set[client]];
            zones.Erase(gI_CurrentZone[client]);
            Call_OnZoneDeleted(client, gI_Set[client], gI_CurrentZone[client]);
            if (zones.Length > 0 && gI_CurrentZone[client] == zones.Length)
                gI_CurrentZone[client]--;
            DisplayMainMenu(client);
            return;
        }
        case 6:
        {
            DisplayMainMenu(client);
            return;
        }
    }
    DisplayEditMenu(client);
}