void DisplayMainMenu(int client)
{
    Menu menu = new Menu(MenuHandler_Main);
    menu.Pagination = MENU_NO_PAGINATION;
    menu.ExitButton = true;

    char title[35];
    FormatEx(title, sizeof(title),
        "Split Menu\nSet: %i/%i\nZones: %i", 
        gI_Set[client] + 1, 
        MAX_SETS, 
        gA_Zones[client][gI_Set[client]].Length);
    menu.SetTitle(title);

    menu.AddItem("next_set", "Next Set", gI_Set[client] + 1 == MAX_SETS ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
    menu.AddItem("prev_set", "Prev Set", gI_Set[client] == 0 ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
    menu.AddItem("add", "Add Zone");
    menu.AddItem("edit", "Edit Zones");

    menu.Display(client, MENU_TIME_FOREVER);
}

int MenuHandler_Main(Menu menu, MenuAction action, int client, int choice)
{
    if (action == MenuAction_End)
        delete menu;
    if (action != MenuAction_Select) return;

    switch (choice)
    {
        case 0:
        {
            gI_Set[client]++;
            DisplayMainMenu(client);
        }
        case 1:
        {
            gI_Set[client]--;
            DisplayMainMenu(client);
        }
        case 2:
        {
            DisplayAddMenu(client);
        }
        case 3:
        {
            DisplayEditMenu(client);
        }
    }
}