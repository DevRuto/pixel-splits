public void GOKZ_OnTimerStart_Post(int client, int course)
{
    Splits_Start(client);
}

public void GOKZ_OnTimerEnd_Post(int client, int course, float time, int teleportsUsed)
{
    Splits_Finish(client, time);
}

public int KZTimer_TimerStarted(int client)
{
    Splits_Start(client);
}

public int KZTimer_TimerStopped(int client, int teleports, float time, int record)
{
    Splits_Finish(client, time);
}