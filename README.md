# Pixel Splits

> Features

* Add Zones
* Edit Zones
* Zones saved to SQLite or MySQL in "pixel" configuration

## Make sure to add "pixel" config in databases.cfg

## TODO

- [ ] Save splits

## HOWTO
1. Add plugin to plugins folder - download page [Downloads](https://bitbucket.org/DevRuto/pixel-splits/downloads/)
2. Add `"pixel"` configuration to `databases.cfg`

    > Example

```
"pixel"
{
    "driver"         "sqlite"
    "database"       "pixel-sqlite"
}
```