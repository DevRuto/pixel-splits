#if defined _pixel_splits_included
 #endinput
#endif
#define _pixel_splits_included

#include <gamechaos>

/**
 * Called when a player adds a zone to their set of zones
 *
 * @param client        Client index
 * @param set           The set index
 * @param zone_index    The index of the zone in the set
 * @param zone          The added zone
 */
forward void Pixel_OnZoneAdded(int client, int set, int zone_index, Zone zone);

/**
 * Called when a player edits an existing zone in their set
 *
 * @param client        Client index
 * @param set           The set index
 * @param zone_index    The index of the zone in the set
 * @param zone          The edited zone
 */
forward void Pixel_OnZoneEdited(int client, int set, int zone_index, Zone zone);

/**
 * Called when a player deletes a zone from their set
 *
 * @param client        Client index
 * @param set           The set index
 * @param zone_index    The index of the deleted zone in the set
 */
forward void Pixel_OnZoneDeleted(int client, int set, int zone_index);

forward void Pixel_OnMapSetup(int mapId);

forward void Pixel_OnClientSetup(int client);

forward void Pixel_OnSplitStart(int client, int set);

forward void Pixel_OnSplit(int client, int set, int zoneIndex, float splitTime, float totalTime);

forward void Pixel_OnSplitFinish(int client, int set, int zoneIndex, int numSplits, const float[] splits, const float[] times);

/**
 * Get the current map ID
 *
 * @return              The map ID of the map
 */
native int Pixel_GetMapID();

/**
 * Get the current set index for the player
 *
 * @param client        Client index
 * @return              The active set index for the player
 */
native int Pixel_GetCurrentSetIndex(int client);

/**
 * Get the active set of zones
 *
 * @param client        Client index
 * @return              The active set of zones
 */
native ArrayList Pixel_GetCurrentSet(int client);

stock char[] ZoneToInfoString(Zone zone)
{
    char info[255];
    Format(
        info,
        sizeof(info),
        "Point 1 { %f, %f, %f } | Point 2 { %f, %f, %f } | Mins { %f, %f, %f } | Maxs { %f, %f, %f }",
        zone.point1[0], zone.point1[1], zone.point1[2],
        zone.point2[0], zone.point2[1], zone.point2[2],
        zone.mins[0], zone.mins[1], zone.mins[2],
        zone.maxs[0], zone.maxs[1], zone.maxs[2]);

    return info;
}

public SharedPlugin __pl_pixel_splits = 
{
	name = "pixel-splits", 
	file = "pixel-splits.smx", 
	#if defined REQUIRE_PLUGIN
	required = 1, 
	#else
	required = 0, 
	#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_pixel_splits_SetNTVOptional()
{
    MarkNativeAsOptional("Pixel_GetMapID");
    MarkNativeAsOptional("Pixel_GetCurrentSetIndex");
    MarkNativeAsOptional("Pixel_GetCurrentSet");
}
#endif