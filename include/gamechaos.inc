
#if defined _gamechaos_stocks_included
	#endinput
#endif
#define _gamechaos_stocks_included

#include <gamechaos/misc>
#include <gamechaos/strings>
#include <gamechaos/tracing>

// From GameChaos Ztopwatch plugin
// https://bitbucket.org/GameChaos/zone-stopwatch

// Modified to my needs

// https://bitbucket.org/GameChaos/zone-stopwatch/src/a2914ef75a271d0127ccd7f4c1448036cf536a4c/ztopwatch.sp#lines-332

enum struct Zone
{
	float point1[3];
	float point2[3];
	float mins[3];
	float maxs[3];
	bool point1_active;
	bool point2_active;
	
	// bottom part of box
	void SetPoint1(int client)
	{
		this.SetBottomPoint(client, this.point1);
		this.point1_active = true;
		CPrintToChat(client, "%s {grey}Point #{lime}1{grey} set!", PREFIX);
		
		if (this.point1_active && this.point2_active)
		{
			CalculateAABBMinsMaxs(this.point1, this.point2, this.mins, this.maxs);
		}
	}
	
	// top part of box
	void SetPoint2(int client)
	{
		this.SetTopPoint(client, this.point2);
		this.point2_active = true;
		CPrintToChat(client, "%s {grey}Point #{lime}2{grey} set!", PREFIX);
		
		if (this.point1_active && this.point2_active)
		{
			CalculateAABBMinsMaxs(this.point1, this.point2, this.mins, this.maxs);
		}
	}
	
	void SendPoint1Square(int client, int modelIndex, const int colour[4])
	{
		SendBeamSquare(client, modelIndex, this.point1, colour);
	}
	
	void SendPoint2Square(int client, int modelIndex, const int colour[4])
	{
		this.point2[2] -= 72;
		SendBeamSquare(client, modelIndex, this.point2, colour);
		this.point2[2] += 72;
	}
	
	void SendBeamZone(int client, int modelIndex, const int colour[4])
	{
		if (this.point1_active && this.point2_active)
		{
			TE_SendBeamBox(client, view_as<float>({ 0.0, 0.0, 0.0 }), this.point1, this.point2, modelIndex, 0, 1.0, 2.0, colour, 2.0);
		}
	}
	
	void SetBottomPoint(int client, float point[3])
	{
		GetClientAbsOrigin(client, point);
		// float fOrigin[3];
		// GetClientEyePosition(client, point);
		// float fAngles[3];
		// GetClientEyeAngles(client, fAngles);
		// float fMins[3] = { -16.0, -16.0, 0.0 };
		// float fMaxs[3] = { 16.0, 16.0, 0.0 };
		
		// TraceHullDirection(fOrigin, fAngles, fMins, fMaxs, point, 256.0);
	}

	void SetTopPoint(int client, float point[3])
	{
		GetClientAbsOrigin(client, point);
		point[2] += 72;
		// float fOrigin[3];
		// GetClientEyePosition(client, point);
		// float fAngles[3];
		// GetClientEyeAngles(client, fAngles);
		// float fMins[3] = { -16.0, -16.0, 0.0 };
		// float fMaxs[3] = { 16.0, 16.0, 0.0 };
		
		// TraceHullDirection(fOrigin, fAngles, fMins, fMaxs, point, 256.0);
	}
	
	// Reset zone positions
	void Reset()
	{
		this.point1_active = false;
		this.point2_active = false;
		this.point1 = NULL_VECTOR;
		this.point2 = NULL_VECTOR;
	}
}

stock void SendBeamSquare(int client, int modelIndex, const float point[3], const int colour[4])
{
	float vertices[4][3];
	RectangleVerticesFromPoint(vertices, point, view_as<float>({ -16.0, -16.0, 0.0 }), view_as<float>({ 16.0, 16.0, 0.0 }));
	
	float width = 2.0;
	float life = 3.0;
	
	// send the square
	for (int i; i < 4; i++)
	{
		int j = (i == 3) ? (0) : (i + 1);
		TE_SetupBeamPoints(vertices[i], vertices[j], modelIndex, 0, 0, 0, life, width, width, 0, 0.0, colour, 0);
		TE_SendToClient(client);
	}
	// make a nice cross
	TE_SendBeamCross(client, point, modelIndex, 0, life, width, colour, width);
}

stock bool AABBIntersecting(float a_mins[3], float a_maxs[3], float b_mins[3], float b_maxs[3])
{
	return (a_mins[0] <= b_maxs[0] && a_maxs[0] >= b_mins[0]) &&
		   (a_mins[1] <= b_maxs[1] && a_maxs[1] >= b_mins[1]) &&
		   (a_mins[2] <= b_maxs[2] && a_maxs[2] >= b_mins[2]);
}

stock void CalculateAABBMinsMaxs(const float point1[3], const float point2[3], float mins[3], float maxs[3])
{
	for (int i; i < 3; i++)
	{
		float new_mins;
		float new_maxs;
		new_mins = point1[i] < point2[i] ? point1[i] : point2[i];
		new_maxs = point1[i] > point2[i] ? point1[i] : point2[i];
		mins[i] = new_mins;
		maxs[i] = new_maxs;
	}
}